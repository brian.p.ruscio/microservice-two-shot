# Wardrobify

Team:

- Ahad - Which microservice?
  Shoes
- Brian - Which microservice?
  Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
My Shoes model has attributes of name, manufacturer, color, picture_url, and bin. The bin attribute is a foreign key to BinVO, and BinVO model gets updated from the Bin model in the wardrobe microservice via polling.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
