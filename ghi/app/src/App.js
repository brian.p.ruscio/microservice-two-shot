import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatList from "./HatsList";
import HatForm from "./HatForm";
import ShoeList from "./ShoeList";
import CreateShoeForm from "./CreateShoeForm";

function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="/hats/new" element={<HatForm />} />
					<Route path="/hats" element={<HatList />} />
					<Route path="shoes">
						<Route index element={<ShoeList />} />
						<Route path="add" element={<CreateShoeForm />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
