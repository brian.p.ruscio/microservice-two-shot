import React, { useEffect, useState } from "react";

function CreateShoeForm() {
	const [bins, setBins] = useState([]);

	const [formData, setFormData] = useState({
		name: "",
		manufacturer: "",
		color: "",
		picture_url: "",
		bin: "",
	});
	const [hasCreated, setHasCreated] = useState(false);

	const fetchData = async () => {
		const url = "http://localhost:8100/api/bins";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setBins(data.bins);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleFormDataChange = async (event) => {
		const name = event.target.name;
		const value = event.target.value;
		setFormData({ ...formData, [name]: value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = {};

		data.name = formData.name;
		data.manufacturer = formData.manufacturer;
		data.color = formData.color;
		data.picture_url = formData.picture_url;
		data.bin = formData.bin;

		console.log(data);

		const shoesUrl = "http://localhost:8080/api/shoes/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(shoesUrl, fetchConfig);
		if (response.ok) {
			const newAttendConference = await response.json();
			console.log(newAttendConference);
			setFormData({
				name: "",
				manufacturer: "",
				color: "",
				picture_url: "",
				bin: "",
			});
			setHasCreated(true);
		}
	};

	let messageClasses = "alert alert-success d-none mb-0";
	let formClasses = "";
	if (hasCreated) {
		messageClasses = "alert alert-success mb-0";
		formClasses = "d-none";
	}

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a shoe to your Wardrobe!</h1>
					<form
						className={formClasses}
						onSubmit={handleSubmit}
						id="create-shoe-form"
					>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								placeholder="Shoe"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
								value={formData.name}
							/>
							<label htmlFor="name">Shoe</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								placeholder="Manufacturer"
								required
								type="text"
								name="manufacturer"
								id="manufacturer"
								className="form-control"
								value={formData.manufacturer}
							/>
							<label htmlFor="manufacturer">Manufacturer</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								placeholder="Color"
								required
								type="text"
								name="color"
								id="color"
								className="form-control"
								value={formData.color}
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								placeholder="Insert a shoe picture url"
								required
								type="text"
								name="picture_url"
								id="picture_url"
								className="form-control"
								value={formData.picture_url}
							/>
							<label htmlFor="picture_url">Picture</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleFormDataChange}
								required
								name="bin"
								id="bin"
								className="form-select"
								value={formData.bin}
							>
								<option value="">Bins</option>
								{bins.map((bin) => {
									return (
										<option key={bin.id} value={bin.id}>
											{bin.closet_name}
										</option>
									);
								})}
							</select>
						</div>
						<button type="submit" className="btn btn-primary">
							Add
						</button>
					</form>
					<div className={messageClasses} id="success-message">
						Shoe added!
					</div>
				</div>
			</div>
		</div>
	);
}

export default CreateShoeForm;
