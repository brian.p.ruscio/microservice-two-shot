import React, { useEffect, useState } from 'react';

function HatForm() {
  const [location, setLocations] = useState([]);
  const [styleName, setStyleName] = useState('');
  const [fabric, setFabric] = useState('');
  const [color, setColor] = useState('');
  const [pictureURL, setPictureURL] = useState('');
  const [Location, setLocationChange] = useState('');

  const handleStyleNameChange = (event) => {
    const value = event.target.value;
    setStyleName(value);
  };

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureURLChange = (event) => {
    const value = event.target.value;
    setPictureURL(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocationChange(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      fabric: fabric,
      style_name: styleName,
      color: color,
      picture_url: pictureURL,
      location: Location,
    };
    console.log(data.location)
    const hatsUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };
  console.log("LOCATIONS", location)
    useEffect(() => {
        fetchData();
    }, []);

    return (
          <div className="row">
          <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add A Hat To Your Wardrobe!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
                <input onChange={handleFabricChange} placeholder="Fabric" required type="text" value={fabric} name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} placeholder="Style Name" required value={styleName}
                type="text" name="style_name" id="style_name"
                className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required value={color} type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureURLChange} placeholder="Picture URL" required value={pictureURL} type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select required name="location" id="location" className="form-select" onChange={handleLocationChange} value={Location}>
                <option value="">Locations</option>
                {location.map(location => {
                  return (
                    <option key={location.href} value={location.href}>
                  {location.closet_name}
                </option>
              );
            })}
          </select>
        </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
      }

export default HatForm;
