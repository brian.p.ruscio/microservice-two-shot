
import React, { useState, useEffect} from 'react';


function HatList() {
  const [hats, setHats]= useState([]);
  const fetchData = async () => {
		const url = "http://localhost:8090/api/hats";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setHats(data.hats);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);
// add delete function here
  const deleteHat = async (id) => {
    const deleteHatURL = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "DELETE",
    };
    const response = await fetch(deleteHatURL, fetchConfig);
    if (response.ok) {
      fetchData();
    } else {
      console.log("ERROR: COULD NOT DELETE SHOE")
    }
  };

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Shoe Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hats, index) => {
            return (
              <tr key={index}>
                <td>{ hats.style_name }</td>
                <td>{ hats.color }</td>
                <td>{ hats.fabric }</td>
                <td>
                  <img className="img-thumbnail" src={hats.picture_url}
                  style={{width: "50px", height: "50px"}}/>
                </td>
                <td> <button onClick={() => deleteHat(hats.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
