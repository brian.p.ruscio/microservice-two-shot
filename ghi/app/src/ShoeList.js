import React, { useState, useEffect } from "react";

function ShoeList() {
	const [shoes, setShoes] = useState([]);
	const fetchData = async () => {
		const url = "http://localhost:8080/api/shoes";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setShoes(data.shoes);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const deleteShoe = async (id) => {
		const deleteUrl = `http://localhost:8080/api/shoes/${id}`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(deleteUrl, fetchConfig);

		if (response.ok) {
			// Refresh the shoe list after successful deletion
			fetchData();
		} else {
			// Handle error if the deletion was not successful
			console.log("Failed to delete shoe");
		}
	};

	return (
		<table className="table table-hover table-striped-columns">
			<colgroup>
				<col style={{ width: "15%" }} />
				<col style={{ width: "15%" }} />
				<col style={{ width: "15%" }} />
				<col style={{ width: "15%" }} />
				<col style={{ width: "15%" }} />
				<col style={{ width: "10%" }} />
			</colgroup>
			<thead className="table-dark">
				<tr>
					<th>Shoe</th>
					<th>Manufacturer</th>
					<th>Color</th>
					<th>Picture</th>
					<th>Bin</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				{shoes.map((shoe, index) => {
					return (
						<tr key={index}>
							<td>{shoe.name}</td>
							<td>{shoe.manufacturer}</td>
							<td>{shoe.color}</td>
							<td>
								<img
									src={shoe.picture_url}
									className="img-fluid"
									alt={shoe.name}
								/>
							</td>
							<td>{shoe.bin.closet_name}</td>
							<td>
								<button onClick={() => deleteShoe(shoe.id)}>Remove</button>
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default ShoeList;
