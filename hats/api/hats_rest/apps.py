from django.apps import AppConfig


class HatsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hats_rest'
