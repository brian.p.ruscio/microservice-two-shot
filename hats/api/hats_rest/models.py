from django.db import models
from django.db.models.options import Options
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=250, unique=True)
    closet_name = models.CharField(max_length=200)

    def __str__(self):
        return self.closet_name

# Create your models here.
class Hats(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=35)
    picture_url = models.URLField()

    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"id": self.id})




