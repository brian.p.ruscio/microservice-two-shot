# Generated by Django 4.0.3 on 2023-06-01 21:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200, unique=True)),
                ('closet_name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Shoes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('manufacturer', models.CharField(max_length=100)),
                ('color', models.CharField(max_length=18)),
                ('picture_url', models.URLField(null=True)),
                ('bin', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='shoes_rest.binvo')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
    ]
