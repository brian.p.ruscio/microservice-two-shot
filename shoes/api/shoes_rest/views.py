from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoes, BinVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
# Create your views here.


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "manufacturer",
    ]


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesDetailEncoder)
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_delete_shoes(request, id):
    count, _ = Shoes.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})
